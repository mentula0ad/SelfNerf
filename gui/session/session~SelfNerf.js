var g_SelfNerf;

init = new Proxy(init, {apply: function(target, thisArg, args) {
    target(...args);

    if (g_IsReplay)
        return;

    g_SelfNerf = new SelfNerf();
}});

onTick = new Proxy(onTick, {apply: function(target, thisArg, args) {
    target(...args);

    if (!global.g_SelfNerf)
        return;

    g_SelfNerf.update();
}});
