/**
 * Adds the SelfNerf counter to the global FPS, realtime counters
 * and ceasefire counter shown in the top right corner.
 */
OverlayCounterTypes.prototype.SelfNerf = class extends OverlayCounter
{
    constructor(...args)
    {

	super(...args);

        // Make sure config values exist
        if (!Engine.ConfigDB_GetValue("user", this.Config))
        {
            Engine.ConfigDB_CreateValue("user", this.Config, "true");
            Engine.ConfigDB_SaveChanges("user");
        }
    }

    get(...args)
    {
        if (!global.g_SelfNerf)
	    return "";

	return sprintf(
	    translate("Next extinction event: %(time)s"),
	    { "time": timeToString(Math.ceil(g_SelfNerf.getRemainingTime() / 1000) * 1000) }
        );
    }

};

OverlayCounterTypes.prototype.SelfNerf.prototype.Config = "selfnerf.countdown";

OverlayCounterTypes.prototype.SelfNerf.IsAvailable = () => true;
