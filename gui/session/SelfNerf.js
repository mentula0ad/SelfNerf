class SelfNerf
{

    constructor()
    {
        this.nextEvent;
        this.events;
        this.deleted;
        this.settings;
        this.init();
    }

    init()
    {
        this.settings = this.getSettings();
        let nextEvent = this.settings.frequency * 60000;
        let events = 0;
        while (nextEvent < g_SimState.timeElapsed)
        {
            events++;
            nextEvent += this.getNextFrequency(events);
        }
        this.nextEvent = nextEvent;
        this.events = events;
    }

    getSettings()
    {
        const options = Engine.ReadJSONFile("gui/options/options~SelfNerf.json");
        const prefix = "selfnerf.";
        let settings = {};
        options.forEach(category => category.options.forEach(option => settings[option.config.substring(prefix.length)] = option.val.toString()));
        for (const key in settings)
        {
            const value = Engine.ConfigDB_GetValue("user", prefix + key);
            if (value.length > 0)
                settings[key] = value;
        }
        return settings;
    }

    getNextFrequency(events)
    {
        return 60000 * this.settings.frequency * Math.pow(0.5, this.settings.frequencyint * events);
    }

    getRemainingTime()
    {
        // Math.max is mathematically not needed, but negative results can
        // be returned when difference is very small, causing clock overflow
        return Math.max(0, this.nextEvent - g_SimState.timeElapsed);
    }

    getDestroyableEntities()
    {
        const listType = this.settings.listtype;
        const list = this.settings.list
              .replace(/ /g, '')
              .split(",")
              .filter(x => x.length > 0);

        return Engine.GuiInterfaceCall("GetPlayerEntities").filter(id => {
            const state = GetEntityState(id);
            if (isUndeletable(state))
                return false;
            if (state.mirage)
                return false;
            if (listType == 0 && list.some(x => hasClass(state, x)))
                return false;
            if (listType == 1 && list.every(x => !hasClass(state, x)))
                return false;
            return true;
        });
    }

    destroy()
    {
        let entities = this.getDestroyableEntities();
        let entitiesSize = entities.length;
        const rate = Math.min(1, this.settings.percent * Math.pow(2, this.settings.percentint * (this.events-1)) / 100);
        const quantity = Math.ceil(entitiesSize * rate);
        let selection = [];

        for (let i=0; i<quantity; i++)
        {
            let index = Math.floor(Math.random()*entitiesSize);
            selection.push(entities[index]);
            entities.splice(index, 1);
            entitiesSize--;
        }

        Engine.PostNetworkCommand({
	    "type": "delete-entities",
	    "entities": selection
	});

        this.deleted = quantity;
    }

    update()
    {
        if (!this.nextEvent)
            return;

        if (g_SimState.timeElapsed < this.nextEvent)
            return;

        this.events++;
        this.destroy();
        this.nextEvent += this.getNextFrequency(this.events);

        // We don't use this.settings.notify hereafter, but we read
        // from the config database instead. This will allow users
        // to toggle the notification option from the in-game options menu
        if (Engine.ConfigDB_GetValue("user", "selfnerf.notify") === "true")
            this.notify();
    }

    notify()
    {
        g_NetMessageTypes["selfnerf"]({"entities": this.deleted});
    }

}
