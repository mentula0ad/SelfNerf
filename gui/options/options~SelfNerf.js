init = new Proxy(init, {apply: function(target, thisArg, args) {
    target(...args);

    const options = Engine.ReadJSONFile("gui/options/options~SelfNerf.json");
    g_Options = g_Options.concat(options);

    // TODO: translation. The expected command (below) raises an error
    // translateObjectKeys(g_Options, ["label", "tooltip"]);

    placeTabButtons(
	g_Options,
	false,
	g_TabButtonHeight,
	g_TabButtonDist,
	selectPanel,
	displayOptions
    );

    // Write default values if missing
    let settings = {};
    options.forEach(category => category.options.forEach(option => settings[option.config] = option.val.toString()));
    Object.keys(settings).filter(key => !Engine.ConfigDB_GetValue("user", key)).forEach(key => Engine.ConfigDB_CreateValue("user", key, settings[key]));
    Engine.ConfigDB_SaveChanges("user");

}});
