# About SelfNerf

*SelfNerf* is a [0 A.D.](https://play0ad.com) mod that provokes random extinctions in your civilization.

# Is it serious?

Most likely, no player will ever use it in a teamgame, although it could be fun! However, scenarios or campaigns can be designed on top of it. Also, it can be used to train and challenge ourselves under adverse conditions.

# Features

- Set the reduction rate and the frequency of extinctions.
- Intensify or decrease reduction rate and time frequency after every extinction event.
- Select entities that can or cannot be destroyed.

# Installation

[Click here](https://gitlab.com/mentula0ad/SelfNerf/-/releases/permalink/latest/downloads/selfnerf.pyromod) to download the latest release. Install following the official 0 A.D. guide: [How to install mods?](https://trac.wildfiregames.com/wiki/Modding_Guide#Howtoinstallmods)

_Alternative downloads:_ [Latest Release (.pyromod)](https://gitlab.com/mentula0ad/SelfNerf/-/releases/permalink/latest/downloads/selfnerf.pyromod) | [Latest Release (.zip)](https://gitlab.com/mentula0ad/SelfNerf/-/releases/permalink/latest/downloads/selfnerf.zip) | [Older Releases](https://gitlab.com/mentula0ad/SelfNerf/-/releases)

# Questions & feedback

For more information, questions and feedback, visit the [thread on the 0 A.D. forum](https://wildfiregames.com/forum/topic/106652-selfnerf-mod-a-mod-that-provokes-random-extinctions-in-your-civilization/).
